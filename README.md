# Nautilus

Nautilus is a set of bots made to play the board game "Captain Sonar"
It is composed of :

## Nemo : The Captain / Engineer / First mate
Tries to make the best possible decisions

## Sauron : The Radio Operator
Pinpoints the position of the enemy submarine

## Nautilus : The complete crew
Makes all the bots work together in order to overpower your opponents

# Project status
For now, only Sauron works