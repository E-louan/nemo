import maps


class Sauron:
    def __init__(self, map) -> None:
        self.map = map
        self.possibilities = map.initial_possibilities
        self._in_sector = [
            [0 for j in range(self.map.grid_size)] for i in range(self.map.grid_size)
        ]
        for k, l in enumerate(self.map.sectors):
            for i, j in l:
                self._in_sector[i][j] = k

    def _dist(self, c1, c2):
        (i1, j1), (i2, j2) = c1, c2
        return abs(i1 - i2) + abs(j1 - j2)

    def _to_col(self, l):
        return ord(l) - ord("A")

    def update(self, dir):
        to_del = []
        to_add = []
        if dir in ["S", "N", "W", "E"]:
            for k, l in enumerate(self.possibilities):
                (i, j) = l[-1]
                if dir == "S":
                    if (
                        i == self.map.grid_size - 1
                        or self.map.grid[i + 1][j] == 1
                        or (i + 1, j) in l
                    ):
                        to_del.append(k)
                        continue
                    l.append((i + 1, j))
                elif dir == "E":
                    if (
                        j == self.map.grid_size - 1
                        or self.map.grid[i][j + 1] == 1
                        or (i, j + 1) in l
                    ):
                        to_del.append(k)
                        continue
                    l.append((i, j + 1))
                elif dir == "N":
                    if i == 0 or self.map.grid[i - 1][j] == 1 or (i - 1, j) in l:
                        to_del.append(k)
                        continue
                    l.append((i - 1, j))
                elif dir == "W":
                    if j == 0 or self.map.grid[i][j - 1] == 1 or (i, j - 1) in l:
                        to_del.append(k)
                        continue
                    l.append((i, j - 1))
        elif dir[:2] == "Si":
            for l in self.possibilities:
                (i, j) = l[-1]
                for p in range(1, 5):
                    if i + p == self.map.grid_size or self.map.grid[i + p][j] == 1:
                        break
                    if (i + p, j) in l:
                        continue
                    to_add.append([e for e in l])
                    to_add[-1].append((i + p, j))
                for p in range(1, 5):
                    if j + p == self.map.grid_size or self.map.grid[i][j + p] == 1:
                        break
                    if (i, j + p) in l:
                        continue
                    to_add.append([e for e in l])
                    to_add[-1].append((i, j + p))
                for p in range(1, 5):
                    if i - p == -1 or self.map.grid[i - p][j] == 1:
                        break
                    if (i - p, j) in l:
                        continue
                    to_add.append([e for e in l])
                    to_add[-1].append((i - p, j))
                for p in range(1, 5):
                    if j - p == -1 or self.map.grid[i][j - p] == 1:
                        break
                    if (i, j - p) in l:
                        continue
                    to_add.append([e for e in l])
                    to_add[-1].append((i, j - p))
                l.append((i, j))
        elif dir[:2] == "Su":
            _, sect = dir.split()
            new = []
            seen = [
                [False for _ in range(self.map.grid_size)]
                for _ in range(self.map.grid_size)
            ]
            for l in self.possibilities:
                (i, j) = l[-1]
                if seen[i][j]:
                    continue
                seen[i][j] = True
                new.append([(i, j)])
            self.possibilities = new
            self.update("Dr " + sect + " y")
        elif dir[0] == "D":
            _, s, yn = dir.split()
            s = int(s) - 1
            yn = yn == "y"
            for i, l in enumerate(self.possibilities):
                if (
                    l[-1] in self.map.sectors[s]
                    and not yn
                    or l[-1] not in self.map.sectors[s]
                    and yn
                ):
                    to_del.append(i)
        elif dir[:2] == "So":
            _, col, lin = dir.split()
            col = self._to_col(col)
            lin = int(lin) - 1
            for i, l in enumerate(self.possibilities):
                if l[-1][0] == lin and l[-1][1] == col:
                    to_del.append(i)
                elif l[-1][0] != lin and l[-1][1] != col:
                    to_del.append(i)
        elif dir[0] in ["T", "M"]:
            _, col, lin, dmg = dir.split()
            col = self._to_col(col)
            lin = int(lin) - 1
            dmg = int(dmg)
            for i, l in enumerate(self.possibilities):
                if (
                    self._dist(l[-1], (lin, col)) == 1
                    and dmg != 1
                    or self._dist(l[-1], (lin, col)) == 0
                    and dmg != 2
                    or self._dist(l[-1], (lin, col)) > 1
                    and dmg != 0
                ):
                    to_del.append(i)
        else:
            return
        for k in sorted(to_del, reverse=True):
            self.possibilities.pop(k)
        for e in to_add:
            self.possibilities.append(e)

    def best_drone(self):
        nb = [0 for _ in range(self.map.nb_sectors)]
        n = 0
        seen = [
            [False for _ in range(self.map.grid_size)]
            for _ in range(self.map.grid_size)
        ]
        for l in self.possibilities:
            (i, j) = l[-1]
            if seen[i][j]:
                continue
            seen[i][j] = True
            nb[self._in_sector[i][j]] += 1
            n += 1
        mini = 0
        mine = abs(nb[0] - n / 2)
        for i, e in enumerate(nb):
            tmp = abs(e - n / 2)
            if tmp < mine:
                mini = i
        return mini + 1


if __name__ == "__main__":
    translate = "ABCDEFGHIJKLMNO"
    s = Sauron(maps.alpha)
    while True:
        dir = input()
        if dir[0] == "?":
            print(s.best_drone())
            continue
        s.update(dir)
        seen = [[False for j in range(s.map.grid_size)] for i in range(s.map.grid_size)]
        for l in s.possibilities:
            (i, j) = l[-1]
            if seen[i][j]:
                continue
            seen[i][j] = True
            sect = s._in_sector[i][j] + 1
            i = i + 1
            j = translate[j]
            print(i, j, sect)
